import java.util.Random;

//Clase Autómata Celular
public class CA{

  //Variable para definir el estado de vida del NPC. True para vivo, false para muerto.
  int tipo;
  int posX;
  int posY;
  
  float influenciaPositiva;
  float influenciaNegativa;
  
  // 0: Nervioso 1: Calmado 2: Triste 3: Feliz -1: estado de equilibrio
  int emocion = -1;
  
  //Va a ser usado solo por el tipo depredador
  int tiempovida;
 
  //Constructor para los NPC iniciales con el estado vivo/muerto
  public CA(int estado, int x, int y) {
    tipo = estado;
    posX = x;
    posY = y;
    influenciaPositiva= 0.0;
    influenciaNegativa = 0.0;
  }
  
  public void setTipo(int estado){ 
    
    if(estado==tipo){ //Si el estado es el mismo, no es necesario obtener nuevas influencias
      tipo = estado;
    }
    
    else{
      tipo = estado;
      
      if(estado == 2){ //depredador
        tiempovida = 3;
      }
      else{ //individuo
        influenciaPositiva = aleatorio();
        delay(50);
        influenciaNegativa = aleatoriosr();
      }

    }
  }
  
  //retorna la posición x del automata
  public int getposx(){
    return posX;
  }
  
  //retorna la posición y del automata
  public int getposy(){
    return posY;
  }
  
  public int getTipo(){
    return tipo;
  }
  
  public float aleatorio(){
    Random random = new Random(System.currentTimeMillis());
    
    return random.nextFloat();
  }
  
  public float aleatoriosr(){
    Random random = new Random();
    return random.nextFloat();
  }
  
  public int getTiempovida(){
    return tiempovida;
  }
  
  public void disminuirTiempovida(){
    if(tiempovida==1){
      tipo = 0;
    }
    else{
      tiempovida -= 1;
    }
  }
  
  public int getemocion(){
    return emocion;
  }
  public void setemocion(int valor){
    emocion = valor;
  }
  
  public float getinfluenciapositiva(){
    return influenciaPositiva;
  }
  
  public float getinfluencianegativa(){
    return influenciaNegativa;
  }
  
}