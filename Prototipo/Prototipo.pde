//Libreria para msgbox (alerta)
import static javax.swing.JOptionPane.*;

//Objeto Controlador
Controlador controlador;

//metodo de inicializacion de la aplicacion
void setup(){
  //setea la aplicacion con pantalle completa
 
  //fullScreen();
  size(1280,720);
  
  //inicializamos el controlador
  controlador = new Controlador();
  
  //suaviza los elementos gráficos
  smooth();
}
//metodo  de reproduccion ciclica
void draw(){
  //setea el fondo de color 
  background(0,0,0);
  //elimina el borde de los elementos gráficos
  //noStroke();
  strokeWeight(0.1);
  controlador.show();
  
} 
// Una vez se detecta evento en en el mouse se ejecuta
void mouseClicked() {
  controlador.mouseClicked();
}

void keyPressed(){
  controlador.keyPressed();
}